from pathlib import Path
import json
import yaml

this = Path(__file__)
local = this.parent.parent

catpath = local / "categories"
by_id = {}
for path in sorted(catpath.glob("*.yaml")):
    with open(path) as fin:
        d = yaml.load(fin, Loader=yaml.FullLoader)
        by_id[f"category{d['category_id']:04}"] = d
with open(local / "categories.json", "w") as fout:
    print (json.dumps(by_id, indent=2), file=fout)