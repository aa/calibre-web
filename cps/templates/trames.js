  {%- if entries and entries[0] -%}
jsonbooks={ 
{%- for entry in entries -%}
{{entry.id}}:{'len':{{entry.data | length}},'cat':[{% for tag in entry.tags%}{{tag.id}}{{ "," if not loop.last }}{% endfor %}],'lang':[{% for lang in entry.languages %}'{{ lang.lang_code }}'{{ "," if not loop.last }}{% endfor %}]}{{ "," if not loop.last }}
  {%- endfor -%}
};
  {% endif %}
thickness={1:'2px',2:'8px',3:'14px',4:'18px'};
languages={'eng':'#58428B','nld':'#3ae1c5','fra':'#ffB2B2','deu':'#ff312E','spa':'#00BF69'};
langdeg=['0deg','22.5deg','45deg','-45deg','-22.5deg'];

function update_cover (cover_elt) {
	const cover_over = document.createElement("div");
	idbook=parseInt(cover_elt.querySelector('a').getAttribute('href').split('/book/')[1]);
	cover_elt.dataset.id=idbook;
	//cover_elt.querySelector('img').style.opacity=0;
	//cover_elt.addEventListener('mouseenter',function(){
	//	this.querySelector('img').style.opacity=1;
	//});
	//cover_elt.addEventListener('mouseleave',function(){
	//	this.querySelector('img').style.opacity=0;
	//});
	cover_over.style.opacity = 1;
	cover_elt.addEventListener('mouseenter',function(){
		cover_over.style.opacity=0;
	});
	cover_elt.addEventListener('mouseleave',function(){
		cover_over.style.opacity=1;
	});

	cover_elt.querySelector("span.img").style.position = "relative";
	const cover_span=cover_elt.querySelector('.img');
	cover_over.style.position= "absolute";
	cover_over.style.left = "0";
	cover_over.style.top = "0";
	cover_over.style.clipPath = "polygon(0% 0%, 0% 100%, 100% 100%)";
	cover_over.style.background = "white";
	cover_over.style.width = "100%";
	cover_over.style.height = "100%";

	cover_span.appendChild(cover_over);
	cover_over.classList.add("overlay");
	cover_span.style.display='inline-block';
	cover_span.style.height='100%';
	const thick=jsonbooks[idbook]['len'];
	const cat=jsonbooks[idbook]['cat'];
	let langs=jsonbooks[idbook]['lang'];
	if (langs.length == 0) {
		langs=['eng'];
	}
	if (cat.includes(1)) { 
		cssline='';
		for (var j=0;j<langs.length;j++){
			cssline+='repeating-linear-gradient( '+langdeg[j]+', ';
			if (languages.hasOwnProperty(langs[j])) {
				var color=languages[langs[j]];
			}else{
				var color='black';
			}
			if (j==langs.length-1){
				/* this is the solid background EEE */
			        cssline+=color+', '+color+' '+thickness[thick]+', #EEE 0px, #EEE 50px';
				cssline+=')';
			}else{
			        cssline+=color+', '+color+' '+thickness[thick]+', transparent 0px, transparent 50px';
				cssline+='),';
			}
		}
		cover_over.style.background=cssline;
	}
	else { 
		cssline='';
		for (var j=0;j<langs.length;j++){
		cssline+='radial-gradient( circle at 50% calc(100%/'+(j+2)+'), ';
			if (languages.hasOwnProperty(langs[j])) {
				var color=languages[langs[j]];
			}else{
				var color='black';
			}
			if (j==langs.length-1){
				cssline+=color+' '+thick*2+'px, #EEE 0px';
				cssline+=')';
			}else{
			 	cssline+=color+' '+thick*2+'px, transparent 0px';
				cssline+='),';
			}
		}
		cover_over.style.background=cssline;
		cover_over.style.backgroundSize=(15+j)+'px '+(30+(j*5))+'px';
		cover_over.style.backgroundPosition='center';
	}
	// console.log(cssline);
}

document.addEventListener('DOMContentLoaded',function(){
	allcovers=document.querySelectorAll('.discover .cover');
	for (var i=0;i<allcovers.length;i++) {
		update_cover(allcovers[i]);
	}
});

// {michael, jan 2024}
// watch for dynamically loaded elements, call update_cover on them as well
// following https://stackoverflow.com/questions/3219758/detect-changes-in-the-dom#14570614
var observeDOM = (function(){
	var MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
  
	return function( obj, callback ){
	  if( !obj || obj.nodeType !== 1 ) return; 
  
	  if( MutationObserver ){
		// define a new observer
		var mutationObserver = new MutationObserver(callback)
  
		// have the observer observe for changes in children
		mutationObserver.observe( obj, { childList:true, subtree:true })
		return mutationObserver
	  }
	  
	  // browser support fallback
	  else if( window.addEventListener ){
		obj.addEventListener('DOMNodeInserted', callback, false)
		obj.addEventListener('DOMNodeRemoved', callback, false)
	  }
	}
  })();
// watch the parent of the first book (div itself doesn't have any good id/class :( )
const books_container = document.querySelector(".book").parentElement;
if (books_container) {
	// console.log("watching for changes", books_container);
	observeDOM(books_container, function (mod) {
		mod.forEach(function (record) {
			// console.log("added", record.addedNodes);
			record.addedNodes.forEach(function (elt) {
				if (elt.classList.contains("book")) {
					const cover = elt.querySelector(".cover");
					if (cover) { 
						update_cover(cover);
					}
				}
			})
			// console.log("removed", record.removedNodes);
		})
	})	
}
// {/michael, jan 2024}
