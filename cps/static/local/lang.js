console.log("local lang.js");
(function () {
    const lang = document.querySelector("html").lang;
    console.log("lang", lang);
    for (let elt of document.querySelectorAll("*[lang]")) {
	const elt_lang = elt.getAttribute("lang");
	console.log("elt", elt, elt_lang);
	if (lang && elt_lang) {
	    if (lang != elt_lang) {
		elt.style.display = "none";
	    }
	}
    }
})();
