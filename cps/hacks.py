from . import constants

# evt. migrate this out to a configuration file

SIDEBAR = [
    {'id': 'new', 'sort_order': 0},
    {'id': 'bwaa', 'sort_order': 10, 'create': {'glyph': 'glyphicon-book', 'text': 'Books with an attitude', 'link': 'web.books_list', "id": "bwaa", "visibility": constants.SIDEBAR_RECENT, 'public': True, 'page': 'category', 'show_text': 'Books with an attitude', 'config_show': False}},
    {'id': 'verlag', 'sort_order': 20, 'create': {'glyph': 'glyphicon-book', 'text': 'Verlag', 'link': 'web.books_list', "id": "verlag", "visibility": constants.SIDEBAR_RECENT, 'public': True, 'page': 'category', 'book_id': '8', 'show_text': 'Books with an attitude', 'config_show': False}},
    {'id': 'hot', 'sort_order': 40},
    {'id': 'download', 'sort_order': 50},
    {'id': 'rated', 'sort_order': 60},
    {'id': 'read', 'sort_order': 70},
    {'id': 'unread', 'sort_order': 80},
    {'id': 'rand', 'sort_order': 999},
    {'id': 'cat', 'sort_order': 100},
    {'id': 'serie', 'sort_order': 110},
    {'id': 'author', 'sort_order': 120},
    {'id': 'publisher', 'sort_order': 130},
    {'id': 'lang', 'sort_order': 140},
    {'id': 'rate', 'sort_order': 150},
    {'id': 'format', 'sort_order': 160},
    {'id': 'archived', 'sort_order': 170},
    {'id': 'list', 'sort_order': 180}
]

def sidebar_filter (sidebar):
    """ side bar is a list of dictionaries in the form
    
    {"glyph": "glyphicon-book", "text": _('Books'), "link": 'web.index', "id": "new",
                    "visibility": constants.SIDEBAR_RECENT, 'public': True, "page": "root",
                    "show_text": _('Show recent books'), "config_show":False}
                    
    """
    # Index tweaks by id
    # Append any 'create' items
    tweaks_by_id = {}
    for tweak in SIDEBAR:
        tweaks_by_id[tweak['id']] = tweak
        if 'create' in tweak:
            sidebar.append(tweak['create'])
    # Apply sort_order (& evt. other tweaks)
    for element in sidebar:
        if element['id'] in tweaks_by_id:
            tweak = tweaks_by_id[element['id']]
            element['sort_order'] = tweak.get('sort_order', 9999)
    # reorder items by sort_order
    sidebar.sort(key=lambda x: x['sort_order'])
    
    return sidebar