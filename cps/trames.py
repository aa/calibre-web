# -*- coding: utf-8 -*-

#  This file is part of the Calibre-Web (https://github.com/janeczku/calibre-web)
#    Copyright (C) 2018-2019 OzzieIsaacs, cervinko, jkrehm, bodybybuddha, ok11,
#                            andy29485, idalin, Kyosfonica, wuqi, Kennyl, lemmsh,
#                            falgh1, grunjol, csitko, ytils, xybydy, trasba, vrabe,
#                            ruben-herold, marblepebble, JackED42, SiphonSquirrel,
#                            apetresc, nanu-c, mutschler
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program. If not, see <http://www.gnu.org/licenses/>.

import sys
import platform
import sqlite3
from collections import OrderedDict
from sqlalchemy.sql.expression import func, text, or_, and_, true
from sqlalchemy.exc import InvalidRequestError, OperationalError


import werkzeug
import flask
import flask_login
import jinja2
from flask_babel import gettext as _

from . import db, calibre_db, converter, uploader, constants, dep_check
from .render_template import render_title_template


trames = flask.Blueprint('trames', __name__)

@trames.route("/trames.js")
def trame():
    allbooks = calibre_db.session.query(db.Books).all()
    # allbooks = calibre_db.session.query(db.Authors).join(db.books_authors_link).join(db.Books).filter(calibre_db.common_filters()).all()
    return render_title_template('trames.js', entries=allbooks,  page="trames", mimetype="application/javascript")
