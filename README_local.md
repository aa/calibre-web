# Calibre web (Constant fork)

See [LOCAL_notes.ipynb](LOCAL_notes.ipynb) for specific info.

In general, it's best to use sudo to become the calibre user

    sudo su calibre

Then you can:

    cd /home/calibre/calibre/src/calibre-web  
    git pull
  
19 Jan
--------------

Modifications for trames:

* cps/templates/layout.html, script tag linking trames.js
* cps/trames.py
* cps/templates/trames.js

Trames.js is rendered by Flask and includes a loop that creates JSON in HTML with parallel database of information used by the visualisation for the items on the page. This code however doesn't get triggered when scrolling to a new page (and thus the visualisation only works for the first page).

I wonder if the necessary data could be better incorporated in the actual page (though this means making modifications to a template in the core code).

Otherwise, there would need to be some other means of patching the reloading process.

When scrolling happens, the next page is loaded with an XHR, and presumably the HTML is extracted and spliced into the page, without javascript being triggered.

https://calibre.constantvzw.org/page/2

Alternatively, javascript could monitor the page for dom updates (does this work nowadays?), and dynamically request the additional information necessary + update the visualisation. Maybe this is a better approach (it keeps the same level of intervention, trames.py could perhaps then better work as a static JS + a dynamic api call returning JSON ipv JS).

Another small bug is that the use of render_template to render trames.js returns a document with mime type text/html instead of application/javascript. The browser tolerates this, but perhaps it's another push to separate the javascript (static) and the view (json), and avoid needing to call render_template.